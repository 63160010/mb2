import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}
void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme{
  static ThemeData appThemeLinght(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.white,
          )
          ),
          iconTheme:  IconThemeData(
    color: Colors.indigo[900]
    ),
    );
  }
  static ThemeData appThemeDrak(){
    return ThemeData(
      brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
        color: Colors.grey,
        iconTheme: IconThemeData(
        color: Colors.greenAccent,
    )
    ),
      iconTheme:  IconThemeData(
          color: Colors.greenAccent[900]
      ),
    );
  }
}


class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme =APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
        ? MyAppTheme.appThemeLinght()
      : MyAppTheme.appThemeDrak(),
      home: Scaffold(
        appBar: builAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child : Icon(Icons.threesixty),
            onPressed: (){
              setState(() {
                currentTheme == APP_THEME.DARK
                    ? currentTheme = APP_THEME.LIGHT
                    : currentTheme = APP_THEME.DARK;
              });
            },
          ),
        ),
    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.chat,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}
Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("086-850-9307"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      // color: Colors.indigo.shade500,
      onPressed: (){},
    ),
  );
}
Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text("088-100-2593"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      // color: Colors.indigo.shade500,
      onPressed: (){},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.mail),
    title: Text("63160010@go.buu.ac.th"),
    subtitle: Text("work"),
    // trailing: IconButton(
    //   icon: Icon(Icons.message),
    //   color: Colors.indigo.shade500,
    //   onPressed: () {},
    // ),
  );
}
Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("234 sunnet St,Burlingame"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      // color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

AppBar builAppBarWidget(){
  return AppBar(
      backgroundColor: Colors.black,
      leading: Icon(
          Icons.arrow_back,
           // color: Colors.white
      ),
      actions: <Widget>[
      IconButton(onPressed: (){},
  icon: Icon(
  Icons.star_border),
  // color: Colors.white,
      ),
  ],
  );
}
ListView buildBodyWidget(){
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            // color: Colors.black,
            width: double.infinity,

//Height constraint at Container widget level
            height: 400,

            child: Image.network(
              "https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.6435-9/165530474_1474650549395853_7617529764453584788_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=174925&_nc_ohc=DiEMW4P4TGAAX_uX2Dd&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfDdguX9OM3tWfhPhaE4KSgRCNo2tszh--CVZGG5QSDgdg&oe=63C9F109",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding:EdgeInsets.all(6.0),
                  child:Text("Manutchanok Saetiaw",
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],

            ),
          ),
          Divider(
            // color: Colors.black,
          ),
          Container(
            margin: EdgeInsets.only(top: 8,bottom: 8),
            child: Theme(
              data:ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.greenAccent,
                )
              ),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color: Colors.grey,
          ),
          emailListTile(),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}